from structureddata.treereader import TreeReader

def test():
    'Tests the module.'
    reader = TreeReader()
    with open('H-H.msd', 'r') as fobj:
        tree = reader.read(fobj)
    print(tree)


if __name__ == '__main__':
    test()
