from structureddata.msdparser import MsdParser

def test():
    'Simple module test.'
    parser = MsdParser()
    fobj = open('H-H.msd', 'r')
    parser.parse(fobj)
    fobj.close()


if __name__ == '__main__':
    test()
