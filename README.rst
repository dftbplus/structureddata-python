=======================
StructuredData (python)
=======================

Python framework for reading, manipulating and writing structured
data. Currently, only the machine-readible structured data (MSD) is supported.

The project is 
`hosted on bitbucket <http://bitbucket.org/dftbplus/structureddata-python>`_.

The source code is licensed under the *BSD 2-clause license*.
