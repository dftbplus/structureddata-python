'''Contains an event based parser for MSD-formatted files.
'''
import numpy as np
import structureddata.common as common


class MsdParser:

    '''Event based parser for MSD-formatter files.

    The parser is aware of 3 events: opening a block, closing a block, adding
    data. When those events are encountered during parsing, the appropriate
    routine of the parser is called. In a typical usage, one would replace the
    routines open_block(), close_block(), add_data() with customized routines to
    handle those events.
    '''

    def parse(self, fobj):
        '''Parses an MSD-file.

        The parser processes the file passed to it and calls its routines,
        open_block(), close_block() and add_data() when encountering the
        appropriate events.

        Args:
            fobj (file object): File to parse.
        '''
        line = fobj.readline()
        ind = 0
        while line:
            ind += 1
            firstchar = line[0]
            if firstchar == '{':
                name = line[1:].rstrip()
                self.open_block(name)
            elif firstchar == '}':
                name = line[1:].rstrip()
                self.close_block(name)
            elif firstchar == '@':
                words = line[1:].rstrip().split(':')
                name = words[0]
                datatype = words[1]
                if len(words) == 2:
                    datashape = []
                else:
                    datashape = [int(s) for s in words[2].split(',')]
                    # Make continuous strides in msd-data (Fortran ordering)
                    # remain continuous in numpy (C ordering)
                    # -> shape is reversed *without* changing element order
                    datashape.reverse()
                data = self._handle_data(datatype, datashape, fobj)
                self.add_data(name, data)
            elif firstchar == '#':
                pass
            else:
                msg = 'Unknown data line: "' + line + '"'
                raise common.StructuredDataError(msg)
            line = fobj.readline()


    @staticmethod
    def open_block(name):
        '''Handles a block opening event.

        You should typically override this method with your own implementation
        to do something useful.

        Args:
            name (str): Name of the block which has been opened.
        '''
        print('OPEN: ', name)


    @staticmethod
    def close_block(name):
        '''Handles a block closing event.

        You should typically override this method with your own implementation
        to do something useful.

        Args:
            name (str): Name of the block which has been closed.
        '''
        print('CLOSE: ', name)


    @staticmethod
    def add_data(name, data):
        '''Handles a data adding event

        You should typically override this method with your own implementation
        to do something useful.

        Args:
            name (str): Name of the data which has been added.
            data: Data content. The type of the data can be anything MSD
                supports. It can be either scalar or a numpy array.
        '''
        print('DATA:', name)
        print(data)


    def _handle_data(self, datatype, datashape, fobj):
        if datatype == 'i4':
            data = self._handle_numeric_data(datashape, np.int32, fobj)
        elif datatype == 'i8':
            data = self._handle_numeric_data(datashape, np.int64, fobj)
        elif datatype == 'r4':
            data = self._handle_numeric_data(datashape, np.float32, fobj)
        elif datatype == 'r8':
            data = self._handle_numeric_data(datashape, np.float64, fobj)
        elif datatype == 'c4':
            data = self._handle_complex_data(datashape, np.float32,
                                             np.complex64, fobj)
        elif datatype == 'c8':
            data = self._handle_complex_data(datashape, np.float64,
                                             np.complex128, fobj)
        elif datatype == 'l':
            data = self._handle_logical_data(datashape, fobj)
        elif datatype[0] == 's':
            dtype = 'S' + datatype[1:]
            data = self._handle_string_data(datashape, dtype, fobj)
        else:
            msg = 'Invalid data type \'{}\''.format(datatype)
            raise common.StructuredDataError(msg)
        return data


    @staticmethod
    def _handle_string_data(datashape, dtype, fobj):
        if datashape:
            shape = datashape
            nelems = np.product(shape)
            data = np.empty(nelems, dtype=dtype)
            for ind in range(nelems):
                line = fobj.readline().rstrip()
                data[ind] = line.encode('utf-8')
        else:
            data = fobj.readline().rstrip()
        return data


    @staticmethod
    def _handle_numeric_data(datashape, dtype, fobj):
        if datashape:
            nelems = np.product(datashape)
            data = np.fromfile(fobj, sep=' ', dtype=dtype, count=nelems)
        else:
            data = dtype(fobj.readline().rstrip())
        return data


    def _handle_complex_data(self, datashape, realtype, cmplxtype, fobj):
        if datashape:
            cmplxshape = [2 * datashape[0]] + datashape[1:]
        cmplxshape = [2,] + datashape
        rawdata = self._handle_numeric_data(cmplxshape, realtype, fobj)
        if datashape:
            flatrawdata = rawdata.flatten()
            data = np.empty(np.prod(datashape), dtype=cmplxtype)
            data.real = flatrawdata[0::2]
            data.imag = flatrawdata[1::2]
            data.shape = datashape
        else:
            data = cmplxtype(rawdata[0] + 1j * rawdata[1])
        return data


    @staticmethod
    def _handle_logical_data(datashape, fobj):
        if datashape:
            nelems = np.product(datashape)
            data = np.empty(nelems, dtype=np.bool)
            processed = 0
            while processed < nelems:
                words = fobj.readline().split()
                boolvalues = [word == 'T' for word in words]
                nvalues = len(boolvalues)
                data[processed : processed + nvalues] = boolvalues
                processed += nvalues
            data.shape = datashape
        else:
            data = (fobj.readline().strip() == 'T')
        return data
