'''Common entities used in the package.'''

class StructuredDataError(Exception):
    '''Exception during structured data processing.'''
    pass
