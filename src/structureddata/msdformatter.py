'''Contains a formatter for MSD.
'''
import numpy as np
import structureddata.common as common

_NP_STRING_TYPE = np.dtype('S')
_R4_FORMAT_STR = '{:14.7E}'
_R8_FORMAT_STR = '{:23.15E}'
_I4_FORMAT_STR = '{:d}'
_I8_FORMAT_STR = '{:d}'
_LOGICAL_FORMAT_STR = '{}'


class MsdFormatter:

    '''Formatter to convert a structured tree into MSD-format.'''

    @staticmethod
    def open_block(name):
        '''Returns the MSD-representation of a block-opening event.

        Args:
            name (str): Name of the block which has been opened.
        '''
        return '{' + name + '\n'


    @staticmethod
    def close_block(name):
        '''Returns the MSD-representation of a block-closing event.

        Args:
            name (str): Name of the block which has been opened.
        '''
        return '}' + name + '\n'


    def add_data(self, name, data):
        '''Returns the MSD-representation of a data addition event.

        Args:
            name (str): Name of the data to add
            data: Data content to be added. It can be of any type MSD supports,
                being either scalar or a numpy array.
        '''
        tokens = ['@' + name + ':']
        if isinstance(data, np.ndarray):
            status = self._add_array_data(data, tokens)
        else:
            status = self._add_scalar_data(data, tokens)
        if status:
            msg = 'Could not write data of tag ' + name
            raise common.StructuredDataError(msg)
        return ''.join(tokens)


    @staticmethod
    def _add_scalar_data(data, tokens):
        status = 0
        if isinstance(data, np.float32):
            tokens.append('r4\n' + _R4_FORMAT_STR.format(data) + '\n')
        elif isinstance(data, np.float64):
            tokens.append('r8\n' + _R8_FORMAT_STR.format(data) + '\n')
        elif isinstance(data, np.int32):
            tokens.append('i4\n' + _I4_FORMAT_STR.format(data) + '\n')
        elif isinstance(data, np.int64):
            tokens.append('i8\n' + _I8_FORMAT_STR.format(data) + '\n')
        elif isinstance(data, np.complex64):
            tokens.append('c4\n' + _R4_FORMAT_STR.format(data.real) + ' '
                          + _R4_FORMAT_STR.format(data.imag) + '\n')
        elif isinstance(data, np.complex128):
            tokens.append('c8\n' + _R8_FORMAT_STR.format(data.real) + ' '
                          + _R8_FORMAT_STR.format(data.imag) + '\n')
        elif isinstance(data, bool):
            msg = 'T' if data else 'F'
            tokens.append('l\n' + msg + '\n')
        elif isinstance(data, str):
            tokens.append('s' + str(len(data)) + '\n' + data + '\n')
        else:
            status = 1
        return status


    def _add_array_data(self, data, tokens):
        shapestr = ','.join([str(ind) for ind in data.shape[-1::-1]])
        status = 0
        dtype = data.dtype
        if np.issubdtype(dtype, np.float32):
            tokens.append('r4:' + shapestr + '\n')
            self._add_formatted_array(data, 8, _R4_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, np.float64):
            tokens.append('r8:' + shapestr + '\n')
            self._add_formatted_array(data, 5, _R8_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, np.int32):
            tokens.append('i4:' + shapestr + '\n')
            self._add_formatted_array(data, 10, _I4_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, np.int64):
            tokens.append('i8:' + shapestr + '\n')
            self._add_formatted_array(data, 5, _I8_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, np.complex64):
            tokens.append('c4:' + shapestr + '\n')
            view = data.view(dtype=np.float32)
            self._add_formatted_array(view, 8, _R4_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, np.complex128):
            tokens.append('c8:' + shapestr + '\n')
            view = data.view(dtype=np.float64)
            self._add_formatted_array(view, 5, _R8_FORMAT_STR, tokens)
        elif np.issubdtype(dtype, _NP_STRING_TYPE):
            tokens.append('s' + str(data.itemsize) + ':' + shapestr + '\n')
            self._add_string_array(data, tokens)
        # NOTE: check for bool must come after check for string as string arrays
        # are apparently a subtype of bool (numpy 1.8.2, not sure why)
        elif np.issubdtype(dtype, np.bool):
            tokens.append('l:' + shapestr + '\n')
            symbols = np.where(data, 'T', 'F')
            self._add_formatted_array(symbols, 40, _LOGICAL_FORMAT_STR, tokens)
        else:
            status = 1
        return status


    @staticmethod
    def _add_formatted_array(array, itemsperrow, fmt, tokens):
        fullfmt = " ".join([fmt] * itemsperrow)
        nelems = array.size
        array = array.flat
        for ind in range(0, nelems - itemsperrow + 1, itemsperrow):
            tokens.append(fullfmt.format(*array[ind : ind + itemsperrow]))
            tokens.append('\n')
        remaining = nelems % itemsperrow
        if remaining:
            fullfmt = ' '.join([fmt] * remaining)
            tokens.append(fullfmt.format(*array[nelems - remaining : nelems]))
            tokens.append('\n')


    @staticmethod
    def _add_string_array(data, tokens):
        for string in data:
            tokens.append(string.decode('utf-8'))
            tokens.append('\n')
